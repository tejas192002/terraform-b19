 provider "aws" {
   region = "ca-central-1"
}

terraform {
   backend "s3" {
      region = "ca-central-1"
      bucket = "batch-b19"
      key  = "./terraform.tfstate"
   }
}

data "aws_security_group" "existing_sg" {
   name = "default"
   vpc_id = var.vpc_id
}

resource "aws_security_group_rule" "allow_ssh" {
   type        = var.type_of_role
   to_port     = var.to_port
   from_port   = var.from_port
   protocol    = var.protocol
   cidr_blocks = var.cidr_blocks
   security_group_id = data.aws_security_group.existing_sg.id 
}


resource "aws_security_group_rule" "allow_http" {
   type        = var.type_of_role
   to_port     = var.to_port_http
   from_port   = var.from_port_http
   protocol    = var.protocol_http
   cidr_blocks = var.cidr_blocks_http
   security_group_id = data.aws_security_group.existing_sg.id 
}

resource "aws_instance" "instance" {
   instance_type = var.instance_type
   key_name       = var.key_pair
   ami             =  var.ami_id
   vpc_security_group_ids = [data.aws_security_group.existing_sg.id]


   connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("./universal_key.pem")
      host        =  self.public_ip
      }
   

   provisioner "remote-exec" {
   inline = [
      "sudo yum install httpd -y",
      "sudo systemctl start httpd",
      "sudo systemctl enable httpd"
      ]
   }

   provisioner "local-exec" {
    command = "echo '<h1> Hello batch b19' > index.html"
   }

   provisioner "local-exec" {
    command = "sudo chmod 600 index.html"
   }
   provisioner "file" {
      source = "index.html"
      destination = "/var/www/html/index.html"
   }
}