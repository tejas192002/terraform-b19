variable "reg_s3" {
    default = "ca-central-1"
}
variable "buck_name" {
    default = "batch-19"
}
variable "existing_security_group_name" {
    default = "default"
}
variable "vpc_id" {
    default = "vpc-0437bebecb6842dbc"
}
variable "type_of_role" {
    default = "ingress"
}
variable "to_port" {
    default = 22
}
variable "from_port" {
    default = 22
}
variable "protocol" {
    default = "TCP"
}
variable "cidr_blocks" {
    default = ["0.0.0.0/0"]
}
variable "to_port_http" {
    default = 80
}
variable "from_port_http" {
    default = 80
}
variable "protocol_http" {
    default = "TCP"
}
variable "cidr_blocks_http" {
    default = ["0.0.0.0/0"]
}
variable "instance_type" {
    default = "t2.micro"
}
variable "key_pair" {
    default = "universal_key"
}
variable "ami_id" {
    default = "ami-0156b61643fdfee5c"
}
variable "user" {
    default = "root"
}
variable "private_key" {
    default = "./universal_key.pem"
}
variable "echo_text" {
    default = "<h1> welcome to batch b19 <h/1>"
}
variable "source_file" {
    default = "index.html"
}
variable "destination" {
    default = "/var/www/html/index.html"
}
