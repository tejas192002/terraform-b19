variable "ami" {
    default = "ami-0440d3b780d96b29d"
}

variable "instance_type" {
    default = "t2.micro"
}

variable "ssh_key" {
    default = "universal_key"
}

variable "project" {
    default = "tejas"
}

variable "sg_id" {
    default = "sg-04e2d8d2b6cc017cd"
}

variable "min_size" {
    default = 1
}

variable "max_size" {
    default = 4
}

variable "desired_capacity" {
    default = 2
}

variable "subnets" {
    default = ["subnet-0d7eac498d3a16cc1", "subnet-0edaa0acb23611c45"]
}

variable "env" {
    default = "dev"
}

variable "vpc_id" {
    default = "vpc-075274ccadc5a6112"
}
variable "azs" {
    default = ["us-east-1e", "us-east-1c"]
}