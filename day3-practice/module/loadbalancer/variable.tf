variable "project" {}
variable "env" {}
variable "security_group_id" {}
variable "subnet_ids" {}
variable "autoscaling_group_name_home" {}
variable "autoscaling_group_name_cloths" {}
variable "autoscaling_group_name_grocery" {}
variable "vpc_id" {}
