resource "aws_lb_target_group" "tg_home" {
    name        = "${var.project}-tg-home"
    port     = 80
    protocol  = "HTTP"
    vpc_id = var.vpc_id
    tags = {
        Env = var.env
    }
    health_check {
        path = "/"
    }
}

resource "aws_lb_target_group" "tg_cloths" {
    name        = "${var.project}-tg-cloths"
    port     = 80
    protocol  = "HTTP"
    vpc_id = var.vpc_id
    tags = {
        Env = var.env
    }
    health_check {
        path = "/cloths/"
    }
}

resource "aws_lb_target_group" "tg_grocery" {
    name        = "${var.project}-tg-grocery"
    port     = 80
    protocol  = "HTTP"
    vpc_id = var.vpc_id
    tags = {
        Env = var.env
    }
    health_check {
        path = "/grocery/"
    }
}

resource "aws_lb" "lb" {
    name           = "${var.project}-lb"
    internal        = false
    load_balancer_type = "application"
    security_groups    = [var.security_group_id]
    subnets            = var.subnet_ids
    tags = {
        Env = var.env
    }
}

resource "aws_lb_listener" "home" {
    load_balancer_arn = aws_lb.lb.arn
    port              = "80"
    protocol          = "HTTP"
    default_action {
        type = "forward"
        target_group_arn = aws_lb_target_group.tg_home.arn
    }
}

resource "aws_lb_listener_rule" "cloths_rule" {
    listener_arn = aws_lb_listener.home.arn
    priority      = 101

    condition {
        path_pattern {
            values = ["/cloths*"]
        }
    }

    action {
        type = "forward"
        target_group_arn = aws_lb_target_group.tg_cloths.arn
    }
}

resource "aws_lb_listener_rule" "grocery_rule" {
    listener_arn = aws_lb_listener.home.arn
    priority      = 102

    condition {
        path_pattern {
            values = ["/grocery*"]
        }
    }

    action {
        type = "forward"
        target_group_arn = aws_lb_target_group.tg_grocery.arn
    }
}

resource "aws_autoscaling_attachment" "asg-tg-home" {
    autoscaling_group_name = var.autoscaling_group_name_home
    lb_target_group_arn    = aws_lb_target_group.tg_home.arn
}
resource "aws_autoscaling_attachment" "asg-tg-cloths" {
    autoscaling_group_name = var.autoscaling_group_name_cloths
    lb_target_group_arn    = aws_lb_target_group.tg_cloths.arn
}
resource "aws_autoscaling_attachment" "asg-tg-grocery" {
    autoscaling_group_name = var.autoscaling_group_name_grocery
    lb_target_group_arn    = aws_lb_target_group.tg_grocery.arn
}