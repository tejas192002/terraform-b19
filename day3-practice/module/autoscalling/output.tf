output "asg_home_name" {
    value = aws_autoscaling_group.as_home.name 
}

output "asg_cloths_name" {
    value = aws_autoscaling_group.as_cloths.name 
}

output "asg_grocery_name" {
    value = aws_autoscaling_group.as_grocery.name
}