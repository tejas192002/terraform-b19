resource "aws_launch_configuration" "lc_home" {
    image_id = var.image_id
    instance_type = var.instance_type
    key_name = var.key_pair
    name ="${var.project}-lc_home"
    security_groups = [var.security_group_id]
    user_data = <<-EOF
             #!/bin/bash
             yum install httpd -y
             echo ""<h1> hello friends, welcome to the home page" > /var/www/html/index.html
             systemctl start httpd
             systemctl enable httpd
             EOF
}

resource "aws_launch_configuration" "lc_cloths" {
    image_id = var.image_id
    instance_type = var.instance_type
    key_name = var.key_pair
    name ="${var.project}-lc_cloths"
    security_groups = [var.security_group_id]
    user_data = <<-EOF
             #!/bin/bash
             yum install httpd -y
             mkdir /var/www/html/cloths
             echo ""<h1> hello friends, welcome to the cloths section" > /var/www/html/cloths/index.html
             systemctl start httpd
             systemctl enable httpd
             EOF
}

resource "aws_launch_configuration" "lc_grocery" {
    image_id = var.image_id
    instance_type = var.instance_type
    key_name = var.key_pair
    name ="${var.project}-lc_grocery"
    security_groups = [var.security_group_id]
    user_data = <<-EOF
             #!/bin/bash
             yum install httpd -y
             mkdir /var/www/html/grocery
             echo ""<h1> hello friends, welcome to the grocery page" > /var/www/html/grocery/index.html
             systemctl start httpd
             systemctl enable httpd
             EOF
}



resource "aws_autoscaling_group" "as_home" {
    name = "${var.project}-as-home"
    max_size = var.max_size
    min_size =  var.min_size
    desired_capacity = var.desired_capacity
    availability_zones = var.azs 
    launch_configuration = aws_launch_configuration.lc_home.name
    tag {
        key = "Name"
        value = "home"
        propagate_at_launch = true
    }
}

resource "aws_autoscaling_policy" "as_policy_home" {
    autoscaling_group_name = aws_autoscaling_group.as_home.name
    name                   = "${var.project}-as-policy-home"
    policy_type            = "PredictiveScaling"
    predictive_scaling_configuration {
        metric_specification {
            target_value = 40
            predefined_load_metric_specification {
                predefined_metric_type = "ASGTotalCPUUtilization"
                resource_label         = "app/tejas-lb/778d41231b141a0f/targetgroup/tejas-tg-home/943f017f100becff"
            }
            customized_scaling_metric_specification {
                metric_data_queries {
                    id = "scaling"
                    metric_stat {
                        metric {
                            metric_name = "CPUUtilization"
                            namespace   = "AWS/EC2"
                            dimensions {
                                name = "AutoScalingGroupName"
                                value = aws_autoscaling_group.as_home.name
                            }
                        }
                        stat = "Average"
                    }
                }
            }
        }
    }
}
resource "aws_autoscaling_group" "as_cloths" {
    name = "${var.project}-as-cloths"
    max_size = var.max_size
    min_size =  var.min_size
    desired_capacity = var.desired_capacity
    availability_zones = var.azs 
    launch_configuration = aws_launch_configuration.lc_cloths.name
    tag {
        key = "Name"
        value = "cloths"
        propagate_at_launch = true
    }
}

resource "aws_autoscaling_policy" "as_policy_cloths" {
    autoscaling_group_name = aws_autoscaling_group.as_cloths.name
    name                   = "${var.project}-as-policy-cloths"
    policy_type            = "PredictiveScaling"
    predictive_scaling_configuration {
        metric_specification {
            target_value = 40
            predefined_load_metric_specification {
                predefined_metric_type = "ASGTotalCPUUtilization"
                resource_label         = "app/tejas-lb/778d41231b141a0f/targetgroup/tejas-tg-cloths/943f017f100becff"
            }
            customized_scaling_metric_specification {
                metric_data_queries {
                    id = "scaling"
                    metric_stat {
                        metric {
                            metric_name = "CPUUtilization"
                            namespace   = "AWS/EC2"
                            dimensions {
                                name = "AutoScalingGroupName"
                                value = aws_autoscaling_group.as_cloths.name
                            }
                        }
                        stat = "Average"
                    }
                }
            }
        }
    }
}
resource "aws_autoscaling_group" "as_grocery" {
    name = "${var.project}-as-grocery"
    max_size = var.max_size
    min_size =  var.min_size
    desired_capacity = var.desired_capacity
    availability_zones = var.azs 
    launch_configuration = aws_launch_configuration.lc_grocery.name
    tag {
        key = "Name"
        value = "grocery"
        propagate_at_launch = true
    }
}

resource "aws_autoscaling_policy" "as_policy_grocery" {
    autoscaling_group_name = aws_autoscaling_group.as_grocery.name
    name                   = "${var.project}-as-policy-grocery"
    policy_type            = "PredictiveScaling"
    predictive_scaling_configuration {
        metric_specification {
            target_value = 40
            predefined_load_metric_specification {
                predefined_metric_type = "ASGTotalCPUUtilization"
                resource_label         = "app/tejas-lb/778d41231b141a0f/targetgroup/tejas-tg-home/943f017f100becff"
            }
            customized_scaling_metric_specification {
                metric_data_queries {
                    id = "scaling"
                    metric_stat {
                        metric {
                            metric_name = "CPUUtilization"
                            namespace   = "AWS/EC2"
                            dimensions {
                                name = "AutoScalingGroupName"
                                value = aws_autoscaling_group.as_grocery.name
                            }
                        }
                        stat = "Average"
                    }
                }
            }
        }
    }
}
