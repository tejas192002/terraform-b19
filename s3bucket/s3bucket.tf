provider "aws" {
    region = "ca-central-1"
}

resource "aws_s3_bucket" "s3_bucket" {
    bucket = "tejpasitarg"
    acl = "private"
    tags = {
        tag = "my bucket"
    }
}

resource "aws_iam_user" "user" {
    name = "maruti"
    path = "/"
}

resource "aws_iam_policy" "bucket_policy" {
    name = "bucket_policy_s3"
    description = "this is for demo "

    policy = jsonencode({
        Version = "2012-10-17",
        Statement = [
         {
            Action = "s3:PutBucketPolicy",
            Effect = "Allow"
            Resource = "arn:aws:s3:::tejpasitarg"               
         }
        ]
    })
}
resource "aws_iam_policy_attachment" "attachment" {
   name = "policy_attachment"
   users = [aws_iam_user.iam_user.name]
   policy_arn = aws_iam_policy.bucket_policy.arn

   depends_on = [aws_s3_bucket.s3_bucket]
}
resource "aws_iam_user" "iam_user" {
  name = "tejaspatil"
}

resource "aws_iam_user_login_profile" "login" {
  user                    = aws_iam_user.iam_user.name
  password_reset_required = true
  password_length         = 12
  password_requirements {
    require_lowercase      = true
    require_numbers        = true
    require_symbols        = true
    require_uppercase      = true
    allow_users_to_change_password = true
  }
}