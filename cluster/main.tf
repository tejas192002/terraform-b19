provider "aws" {
    region = "eu-central-1"
}

data "aws_iam_policy_document" "clusterassume-role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "iam-role" {
  name               = "eks-cluster-1"
  assume_role_policy = data.aws_iam_policy_document.clusterassume-role.json
}

resource "aws_iam_role_policy_attachment" "AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.iam-role.name
}


resource "aws_iam_role_policy_attachment" "AmazonEKSVPCResourceController" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = aws_iam_role.iam-role.name
}

resource "aws_eks_cluster" "eks-cluster" {
  name     = "tejas"
  role_arn = aws_iam_role.iam-role.arn

  vpc_config {
    subnet_ids = var.subnet
  }

  
  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.AmazonEKSVPCResourceController,
  ]
}