provider "aws"{
    region = "ca-central-1"
}

resource "aws_iam_user" "iam_user" {
    name = "tejas"
    path = "/system/"

    tags = {
        tag-key = "radhe"
    }
}

resource "aws_s3_bucket" "s3_bucket_policy" {
    bucket = "tejaspasi"
    acl ="private" 

     tags = {
        tag = "tejas"
     }  
}

resource "aws_iam_policy" "bucket_policy" {
    name  = "bucketpolicytejas"
    description = "giving the policy for the user to access the s3 bucket"

    policy = jsonencode({
        Version = "2012-10-17",
        Statement = [
            {
                Action = "s3:PutBucketPolicy",
                Effect  = "Allow",
                Resource = "arn:aws:s3:::tejaspasi"
            }
        ]
    })
}
resource "aws_iam_policy_attachment" "policy_attachment" {
    name = "policy_attachment"
    users = [aws_iam_user.iam_user.name]
    policy_arn = aws_iam_policy.bucket_policy.arn
}

