provider "aws" {
    region = "us-east-1"
}

resource "aws_vpc" "my_vpc" {
    cidr_block = var.vpc_cidr
    tags = {
        Name = "$ {var.project}-vpc"
        env = var.env
    }
}

resource "aws_subnet" "private_subnet" {
    vpc_id = aws_vpc.my_vpc.id
    cidr_block = var.private_subnet_cidr

    tags = {
        env = var.env
        Name = "${var.project}-private-subnet"

    }
}

resource "aws_subnet"  "public_subnet" {
    vpc_id      = aws_vpc.my_vpc.id
    cidr_block  = var.public_subnet_cidr
    map_public_ip_on_launch = true
    tags = {
        env = var.env
        Name = "${var.project}-public-subnet"

    }
}

resource "aws_internet_gateway" "my_igw" {
    vpc_id = aws_vpc.my_vpc.id

    tags = {
        env = var.env
        Name = "${var.project}-igw"
    }
}


resource "aws_route" "igw_route" {
    route_table_id  = aws_vpc.my_vpc.default_route_table_id
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_igw.id
}

resource "aws_security_group" "my_sg" {
    name = "${var.project}-sg"
    description = "allow ssh and httpd"
    vpc_id = aws_vpc.my_vpc.id
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        protocol = "TCP"
        from_port = 22
        to_port = 22
    }
    egress {
        cidr_blocks = ["0.0.0.0/0"]
        protocol = "TCP"
        from_port = 80
        to_port = 80
    }
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        protocol = "-1"
        from_port = 0
        to_port = 0
    }
    depends_on = [aws_internet_gateway.my_igw]
}
resource "aws_instance" "instance_1" {
    ami = var.image_id
    instance_type = var.instance_type
    vpc_security_group_ids = [aws_security_group.my_sg.id]
    tags = {
        env = var.env
        Name = "${var.project}-private-instance"
    }
    subnet_id = aws_subnet.private_subnet.id
}
resource "aws_instance"  "instance_2" {
    ami = var.image_id
    instance_type = var.instance_type
    vpc_security_group_ids = [aws_security_group.my_sg.id]
    tags = {
        env = var.env
        Name = "${var.project}-public-instance"

    }
    subnet_id = aws_subnet.private_subnet.id
}