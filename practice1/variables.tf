variable "vpc_cidr" {
    default = "192.166.0.0/18"

}
variable "private_subnet_cidr" {
    default = "192.166.0.0/20"
}
variable "public_subnet_cidr" {
    default = "192.166.16.0/20"
}
variable "project" {
    default = "t-p-application"
}
variable "env" {
    default = "dev"
}
variable "image_id" {
    default = "ami-0c7217cdde317cfec"
}
variable "instance_type" {
    default = "t2.micro"
}
variable "key_pair" {
    default = "universal_key"
}
